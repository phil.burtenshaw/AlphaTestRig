package com.monitisecreate.internal.colouralpharig;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		SeekBar seekAlpha = (SeekBar) findViewById(R.id.seek_alpha);
		final TextView textAlphaPercent = (TextView) findViewById(R.id.text_alpha_percentage);
		final TextView textAlphaHex = (TextView) findViewById(R.id.text_alpha_hex);
		final FrameLayout frameAlpha = (FrameLayout) findViewById(R.id.frame_alpha);
		final EditText editRGB = (EditText) findViewById(R.id.edit_rgb);
		final Button buttonSetColour = (Button) findViewById(R.id.button_setcolour);
		final ViewGroup viewGroupTestArea = (ViewGroup) findViewById(R.id.viewgroup_test_area);
		seekAlpha.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@SuppressLint("NewApi")
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

				textAlphaPercent.setText(Integer.toString(progress) + "%");
				float alphaPercentage = (progress / (float) 100);
				String hexString = Integer.toHexString(Math.round(alphaPercentage * 255));
				if (hexString.length() == 1) {
					hexString = "0" + hexString;
				}
				textAlphaHex.setText(hexString);

				frameAlpha.setAlpha(alphaPercentage);
			}
		});

		buttonSetColour.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					String entered = editRGB.getText().toString();
					if (!entered.contains("#")) {
						entered = "#" + entered;
					}
					ColorDrawable cD = new ColorDrawable();
					cD.setColor(Color.parseColor(entered));
					viewGroupTestArea.setBackground(cD);
				} catch (Exception e) {
					Toast.makeText(MainActivity.this, "Not a valid HexCode", Toast.LENGTH_SHORT)
							.show();
				}
			}
		});
	}

	public static int convertToHex(int n) {
		return Integer.valueOf(String.valueOf(n), 16);
	}
}
